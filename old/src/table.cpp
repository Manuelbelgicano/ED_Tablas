/**
 * @file table.cpp
 * @author Manuel Gachs Ballegeer
 */

template <class T>
void table<T>::copy(const table &orig) {
	info = orig.info;
	titles.resize(0);
	titles.insert(orig.begin(),orig.end());
	dist = orig.dist;
	return;
}

template <class T>
table<T>::table(const size_t &_k) {
	dist(_k);
}

template <class T>
std::string table<T>::get_info() const {
	return info;
}

template <class T>
bool table<T>::set_info(const std::string &ninfo) {
	bool aux = false;
	switch(ninfo) {
		case 'noname_discrete':
			titles.resize(0);
			info = ninfo;
			aux = true;
			break;
		case 'continuous':
			titles.resize(dist.get_nmod());
			make_ci(*this);
			aux = true;
			break;
		case 'name_discrete':
			titles.resize(dist.get_nmod());
			aux = true;
			break;
		default:
			break;
	}
	return aux;
}

template <class T>
void table<T>::set_nmod(const size_t &_k) {
	titles.resize(_k);
	dist.set_nmod(_k);
	return;
}

template <class T>
void table<T>::add_mod(const size_t &i,const float &x,const unsigned int &n,const T &t) {
	if (i!=-1 && i<dist.get_nmod()) {
		titles[i] = t;
		if (info=="continuous") {
			dist.add_mod(i,0,n);
			make_ci(*this);
		}
		else
			dist.add_mod(i,x,n);
	} else {
		titles.push_back(t);
		dist.add_mod(x,n);
	}
	return;
}
	
template <class T>
void table<T>::subst_mod(const size_t &i) {
	if (i>-1 && i<dist.get_nmod()) {
		titles.erase(titles.begin()+i);
		dist.subst_mod(i);
	}
	return;
}

template <class T>
table<T>& table<T>::operator=(const table<T> &tabl) {
	copy(tabl);
	return *this;
}

std::istream& operator>>(std::istream &is,table<T> &t) {
	size_t _k;
	std::string _info;
	is>>_k>>_info;
	t.set_nmod(_k);
	t.set_info(_info);
	
	T t;
	float x;
	unsigned int n;
	if (t.get_info()!="noname_discrete") {
		if (t.get_info()=="name_discrete") {
			for (size_t i=0;i<k;i++) {
				is>>t>>x>>n;
				t.add_mod(i,x,n,t);
			}
		} else {
			for (size_t i=0;i<k;i++) {
				is>>t>>n;
				t.add_mod(i,0,n,t);
			}
			make_ci(*this);
		}
	} else {
		for (size_t i=0;i<k;i++) {
			is>>x>>n;
			dist.add_mod(i,x,n);
		}
	}
	return is;
}

void make_ci(table<T> &t) {
	float_interval fi;
	if (t.get_info()=="continuous") {
		for (int i=0;i<t.get_nmod();i++) {
			float* mod = t.get_mod(i);
			fi = t.get_tmod(i);
			t.add_mod(i,get_center(fi),mod[1],fi);
		}
	return;
}
