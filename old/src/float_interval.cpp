/**
 * @file float_interval.cpp
 * @author Manuel Gachs Ballegeer
 */

#include<iostream>
#include"float_interval.h"

void set_interval(float_interval &i,const float &cotinf,const float &cotsup, const float &stinf, const float &stsup) {
	i.bot[0] = cotinf;
	i.bot[1] = stinf;
	i.top[0] = cotsup;
	i.top[1] = stsup;

	if (!is_valid(i)) {
		float_interval aux;
		i = aux;
	}
	return;
}

float get_range(const float_interval &i) {
	if (is_valid(i))
		return i.top[0]-i.bot[0];
	else
		return -1;
}

float get_center(const float_interval &i) {
	return get_range(i)/2;
}

bool is_valid(const float_interval &i) {
	bool aux = false;
	if (i.top[0]>i.bot[0])
		aux = true;
	else if (i.top[0]==i.bot[0])
		if (i.top[1]==i.bot[1] && .top[1]==0)
			aux = true;
	return aux;
}

float_interval& operator=(const float_interval &i) {
	float_interval aux;
	set_interval(aux,i.bot[0],i.top[0],i.bot[1],i.top[1]);
	return aux;
}

std::istream& operator>>(std::istream &is,float_interval &i) {
	char aux;
	float cotinf,cotsup,stinf,stsup;

	is>>aux>>cotinf;
	if (aux=='[')
		stinf = 0;
	else
		stinf = 1;

	is>>aux>>cotsup>>aux;
	if (aux==')')
		stsup = 1;
	else
		stsup = 0;
	set_interval(i,cotinf,cotsup,stinf,stsup);
	return is;
}

std::ostream& operator<<(std::ostream &os,const float_interval &i) {
	if (i.bot[1]==1)
		os<<"(";
	else
		os<<"[";

	os<<i.bot[0]<<","<<i.top[0];
	if (i.top[1]==0)
		os<<"]";
	else
		os<<")";
	return os;
}
