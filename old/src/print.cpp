/**
 * @file print.cpp
 * @author Manuel Gachs Ballegeer
 */

#include<string>
#include<iostream>
#include"table.h"
#include"print.h"

void print_table(const table &t,const std::string &mode,const std::string &file) {
	swtich (mode) {
		case 'plain':
			if (file!="none")
				printplain_file(t,file);
			else
				printplain_screen(t);
			break;
		case 'markdown':
			if (file!="none")
				printmd_file(t,file);
			else
				printmd_screen(t);
			break;
		case 'latex':
			if (file!="none")
				printlatex_file(t,file);
			else
				printlatex_screen(t);
			break;
		default:
			std::cout<<"Tipo de tabla no soportado\n";
			break;
	}
	return;
}

void printplain_screen(const table &t) {
	distribution dist = t.get_dist();
	if (t.get_info()=="name_discrete")
