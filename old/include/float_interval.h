/**
 * @file float_interval.h
 * @author Manuel Gachs Ballegeer
 */

#include<iostream>

#ifndef _FLOAT_INTERVAL_H
#define _FLOAT_INTERVAL_H

/**
 * TDA Intervalo de números reales
 * Para indicar si es cerrado, el valor de la segunda componente del intervalo será 0, y 1 para abiertos.
 */
struct float_interval {
	float bot[] = {0,1}		///> Par [valor,cerrado/abierto] de la cota inferior
	float top[] = {1,0}		///> Par [valor,cerrado/abierto] de la cota superior
};

/**
 * @brief Cambia los valores del intervalo
 * @param i Intervalo a modificar
 * @param cotinf Nueva cota inferior
 * @param cotsup Nueva cota superior
 * @param stinf Nuevo estado de la cota inferior (cerrado o abierto)
 * @param stsup Nuevo estado de la cota superior (cerrado o abierto)
*/
void set_interval(float_interval &i,const float &cotinf,const float &cotsup,const float &stinf=1,const float &stsup=0);
/**
 * @brief Calcula la amplitud de un intervalo
 * @param i Intervalo del que se quiere saber la amplitud
 * @return La amplitud del intervalo, -1 en caso de error
 */
float get_range(const float_interval &i);
/**
 * @brief Calcula el centro de un intervalo
 * @param i Intervalo del que se quiere saber el centro
 * @return El centro del intervalo
 * @pre El intervalo debe ser válido
 */
float get_center(const float_interval &i);
/**
 * @brief Calcula si un intervalo es válido.
 * @param i Intervalo que se quiere analizar
 * @return @retval true Si el intervalo es válido y @retval false en cualquier otro caso
 */
bool is_valid(const float_interval &i);
/**
 * @brief Sobrecarga del operador de asignación
 */
float_interval& operator=(const float_interval &i);
/**
 * @brief Sobrecarga del operador de entrada
 * Los intervalos deben de ser de la forma:
 * { [ o ( }cota_inferior,cota_superior{ ) o ] }.
 * @post En caso de errores con los caracteres, se supondrá abierto 
 * inferiormente y cerrado superiormente.
 */
std::istream& operator>>(std::istream &is,float_interval &i);
/**
 * @brief Sobrecarga del operador de salida
 */
std::ostream& operator<<(std::ostream &os,const float_interval &i);

#endif
