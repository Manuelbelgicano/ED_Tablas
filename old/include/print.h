/**
 * @file print.h
 * @author Manuel Gachs Ballegeer
 */

#include<string>
#include"table.h"

#ifndef _PRINT_H
#define _PRINT_H

/**
 * @brief Imprime la tabla en el formato seleccionado
 * @param t Tabla a imprimir
 * @param format Formato en el que se quiere imprimir
 * @param file Fichero en el que se quiere escribir la tabla.
 * 	       Si no se da un fichero, se imprime en pantalla.
 */
void print_table(const table &t,const std::string &mode,const std::string &file="none");

//------------------------------------------SIN FORMATO------------------------------------------
/**
 * @brief Imprime en pantalla una tabla en formato de texto plano
 * @param t Tabla a imprimir
 */
void printplain_screen(const table &t);
/**
 * @brief Imprime en formato de texto plano una tabla de una distribucion
 * @param t Tabla a imprimir
 * @param file Fichero en el que se quiere escribir la tabla
 */
void printplain_file(const table &t,const std::string &file);

//---------------------------------------FORMATO MARKDOWN----------------------------------------
/**
 * @brief Imprime en pantalla una tabla en formato Markdown
 * @param t Tabla a imprimir
 */
void printmd_screen(const table &t);
/**
 * @brief Imprime en formato Markdown una tabla de una distribucion
 * @param t Tabla a imprimir
 * @param file Fichero en el que se quiere escribir la tabla
 */
void printmd_file(const table &t,const std::string &file);

//-----------------------------------------FORMATO LATEX-----------------------------------------
/**
 * @brief Imprime en pantalla una tabla en formato LaTex
 * @param t Tabla a imprimir
 */
void printlatex_screen(const table &t);
/**
 * @brief Imprime en formato LaTex una tabla de una distribucion
 * @param t Tabla a imprimir
 * @param file Fichero en el que se quiere escribir la tabla
 */
void printlatex_file(const table &t,const std::string &file);

#endif
