/**
 * @file continua.cpp
 * @author Manuel Gachs Ballegeer
 * @license GNU GPLv3
 */

#include<algorithm>
#include"continua.h"
#include<fstream>
#include<iostream>

tabla_continua::tabla_continua(const std::vector<float> &valores, unsigned int n) : tabla() {
	float min = *std::min_element(valores.cbegin(),valores.cend());
	float max = *std::max_element(valores.cbegin(),valores.cend());
	float amplitud = (max-min)/n;
	std::pair<float,float> aux (0,min);

	for(size_t i=1;i<=n;i++) {
		aux.first = aux.second;
		aux.second = min+(amplitud*i);
		ii.push_back(aux);
	}

	std::vector<float>::const_iterator it;
	std::vector<unsigned int> frec(n,0);
	
	for (it=valores.cbegin();it!=valores.cend();++it)
		for (size_t i=0;i<ii.size();i++) {
			if (*it<=ii[i].second && *it>=ii[i].first) {
				frec[i]++;
				i = ii.size();
			}
		}
	set_ni(frec);
}

std::vector<float> tabla_continua::get_ci() const {
	std::vector<float> ci;
	std::vector<std::pair<float,float> >::const_iterator it;
	std::pair<float,float> aux;

	for (it=ii.cbegin();it!=ii.cend();++it) {
		aux = *it;
		ci.push_back((aux.first+aux.second)/2);
	}
	return ci;
}

std::vector<float> tabla_continua::get_ai() const {
	std::vector<float> ai;
	std::vector<std::pair<float,float> >::const_iterator it;
	std::pair<float,float> aux;

	for (it=ii.cbegin();it!=ii.cend();++it) {
		aux = *it;
		ai.push_back(aux.second-aux.first);
	}
	return ai;
}

bool tabla_continua::to_markdown(const std::string &file) const {
	std::fstream fout(file);
	bool aux = true;
	if (fout) {
		fout<<"| I_i | n_i | f_i | N_i | F_i | a_i | c_i |\n";
		fout<<"|:---:|:---:|:---:|:---:|:---:|:---:|:---:|\n";

		std::vector<unsigned int> ni = get_ni();
		std::vector<unsigned int> Ni = get_Ni();
		std::vector<float> fi = get_fi();
		std::vector<float> Fi = get_Fi();
		std::vector<float> ai = get_ai();
		std::vector<float> ci = get_ci();

		for (size_t i=0;i<ii.size();i++)
			fout<<"| ("<<ii[i].first<<","<<ii[i].second<<") | "<<ni[i]<<" | "<<fi[i]<<" | "<<Ni[i]<<" | "<<Fi[i]<<" | "<<ai[i]<<" | "<<ci[i]<<" |\n";
	} else {
		std::cout<<"Error abriendo el archivo "<<file<<std::endl;
		aux = false;
	}
	if (!fout) {
		std::cout<<"Error durante la escritura\n";
		aux = false;
	}
	fout.close();
	return aux;
}

bool tabla_continua::to_latex(const std::string &file) const {
	std::fstream fout(file);
	bool aux = true;
	if (fout) {
		fout<<"\\documentclass[11pt]{article}\n";
		fout<<"\\begin{document}\n\n";
		fout<<"\\begin{tabular}{|c|c|c|c|c|c|c|}\n";
		fout<<"\\hline\n";
		fout<<"$I_i$ & $n_i$ & $f_i$ & $N_i$ & $F_i$ & $a_i$ & $c_i$ \\\\ \\hline\n";

		std::vector<unsigned int> ni = get_ni();
		std::vector<unsigned int> Ni = get_Ni();
		std::vector<float> fi = get_fi();
		std::vector<float> Fi = get_Fi();
		std::vector<float> ai = get_ai();
		std::vector<float> ci = get_ci();

		for (size_t i=0;i<ii.size();i++)
			fout<<"("<<ii[i].first<<","<<ii[i].second<<") & "<<ni[i]<<" & "<<fi[i]<<" & "<<Ni[i]<<" & "<<Fi[i]<<" & "<<ai[i]<<" & "<<ci[i]<<" \\\\ \\hline\n";
		fout<<"\\end{tabular}\n\n\\end{document}";
	} else {
		std::cout<<"Error abriendo el archivo "<<file<<std::endl;
		aux = false;
	}
	if (!fout) {
		std::cout<<"Error durante la escritura\n";
		aux = false;
	}
	fout.close();
	return aux;
}
