/**
 * @file tabla.cpp
 * @author Manuel Gachs Ballegeer
 * @license GNU GPLv3
 */

#include"tabla.h"

unsigned int tabla::get_poblacion() const {
	unsigned int poblacion = 0;

	for (size_t i=0;i<ni.size();i++)
		poblacion += ni[i];
	return poblacion;
}

std::vector<unsigned int> tabla::get_Ni() const {
	std::vector<unsigned int> Ni;
	unsigned int aux = 0;
	
	for (size_t i=0;i<ni.size();i++) {
		aux += ni[i];
		Ni.push_back(aux);
	}
	return Ni;
}

std::vector<float> tabla::get_fi() const {
	std::vector<float> fi;
	unsigned int poblacion = get_poblacion();

	for (size_t i=0;i<ni.size();i++)
		fi.push_back((float) ni[i]/poblacion);
	return fi;
}

std::vector<float> tabla::get_Fi() const {
	std::vector<float> Fi;
	float aux = 0;
	unsigned int poblacion = get_poblacion();

	for (size_t i=0;i<ni.size();i++) {
		aux += (float) ni[i]/poblacion;
		Fi.push_back(aux);
	}
	return Fi;
}
