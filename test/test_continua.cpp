/**
 * @file test_continua.cpp
 * @author Manuel Gachs Ballegeer
 * @license GNU GPLv3
 */

#include"continua.h"
#include<iostream>

int main() {
	std::vector<float> valores = {1,2,2,3,4,4,5,6,7,7,7,8,9,10};
	std::cout<<"Valores: ";
	for (size_t i=0;i<valores.size();i++)
		std::cout<<valores[i]<<" ";
	std::cout<<std::endl;

	tabla_continua t(valores,5);
	if (!t.to_markdown("salida.md"))
		std::cout<<"Error con Markdown\n";
	if (!t.to_latex("salida.tex"))
		std::cout<<"Error con LaTeX\n";
	return 0;
}
