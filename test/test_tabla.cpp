/**
 * @file test_tabla.cpp
 * @author Manuel Gachs Ballegeer
 * @license GNU GPLv3
 */

#include<iostream>
#include"tabla.h"

int main() {
	std::vector<unsigned int> ni = {2,3,4,5,6,7};
	std::cout<<"Valores: ";
	for (size_t i=0;i<ni.size();i++) std::cout<<ni[i]<<" ";

	tabla t(ni);
	std::cout<<"\nTamaño de la poblacion: "<<t.get_poblacion();

	std::vector<unsigned int> Ni = t.get_Ni();
	std::cout<<"\nValores Ni: ";
	for (size_t i=0;i<Ni.size();i++) std::cout<<Ni[i]<<" ";
	
	std::vector<float> fi = t.get_fi();
	std::cout<<"\nValores fi: ";
	for (size_t i=0;i<fi.size();i++) std::cout<<fi[i]<<" ";
	
	std::vector<float> Fi = t.get_Fi();
	std::cout<<"\nValores Fi: ";
	for (size_t i=0;i<Fi.size();i++) std::cout<<Fi[i]<<" ";
	std::cout<<std::endl;
	return 0;
}
