/**
 * @file discreta.h
 * @author Manuel Gachs Ballegeer
 * @license GNU GPLv3
 */

#ifndef _DISCRETA_H
#define _DISCRETA_H

#include"tabla.h"

template <class T>
class tabla_discreta : class tabla {
	private:
		std::vector<T> xi; ///< Modalidades observadas
	public:
		/**
		 * @brief Constructor a partir de un conjunto de valores
		 * @param valores Conjunto de valores observados
		 */
		tabla_discreta_cuantitativa(const std::vector<T> &valores);
		/**
		 * @brief Constructor a partir de una distribución de frecuencias
		 * @param _ni Frecuencias absolutas
		 * @param _xi Modalidades
		 */
		tabla_discreta(const std::vector<unsigned int> &_ni,const std::vector<T> &_xi);
		/**
		 * @brief Calcula el número de modalidades de la distribución
		 * @return El número de modalidades de la distribución
		 */
		unsigned int get_modalidades() const;
		/**
		 * @brief Muestra la tabla en formato Markdown
		 * @param file Fichero donde se guarda la salida
		 * @return @retval true Si la operación se realiza con éxito
		 * 	   @retval false Si ocurre un error durante la operación
		 */
		bool to_markdown() const;
		/**
		 * @brief Muestra la tabla en formato latex
		 * @param file Fichero donde se guarda la salida
		 * @return @retval true Si la operación se realiza con éxito
		 * 	   @retval false Si ocurre un error durante la operación
		 */
		bool to_latex() const;
};

#include"discreta.cpp"
#endif
