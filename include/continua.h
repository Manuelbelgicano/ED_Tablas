/**
 * @file continua.h
 * @author Manuel Gachs Ballegeer
 * @license GNU GPLv3
 */

#ifndef _CONTINUA_H
#define _CONTINUA_H

#include"tabla.h"
#include<string>
#include<utility>

class tabla_continua : public tabla {
	private:
		std::vector<std::pair<float,float> > ii; ///< Intervalos de la distribución
	public:
		/**
		 * @brief Constructor de la tabla a partir de un conjunto de valores
		 * @param valores Conjunto de valores observados
		 * @param n Número de intervalos
		 */
		tabla_continua(const std::vector<float> &valores, unsigned int n);
		/**
		 * @brief Constructor de la tabla a partir de una distribución de frecuencias
		 * @param _ni Frecuencias absolutas
		 * @param _ii Intervalos de valores observados
		 */
		tabla_continua(const std::vector<unsigned int> &_ni,const std::vector<std::pair<float,float> > &_ii) : tabla(_ni) { ii = _ii; }
		/**
		 * @brief Calcula los representantes de clase de los intervalos
		 * @return Los representantes de clase de los intervalos
		 */
		std::vector<float> get_ci() const;
		/**
		 * @brief Calcula la amplitud de los intervalos
		 * @return La amplitud de los intervalos
		 */
		std::vector<float> get_ai() const;
		/**
		 * @brief Calcula el número de modalidades de la distribución
		 * @return El número de modalidades de la distribución
		 */
		unsigned int get_modalidades() const { return ii.size(); }
		/**
		 * @brief Muestra la tabla en formato Markdown
		 * @param file Fichero donde se guarda la salida
		 * @return @retval true Si la operación se realiza con éxito
		 * 	   @retval false Si ocurre un error durante la operación
		 */
		bool to_markdown(const std::string &file) const;
		/**
		 * @brief Muestra la tabla en formato latex
		 * @param file Fichero donde se guarda la salida
		 * @return @retval true Si la operación se realiza con éxito
		 * 	   @retval false Si ocurre un error durante la operación
		 */
		bool to_latex(const std::string &file) const;
};

#endif
