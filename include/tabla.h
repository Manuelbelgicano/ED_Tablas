/**
 * @file tabla.h
 * @author Manuel Gachs Ballegeer
 * @license GNU GPLv3
 */

#ifndef _TABLA_H
#define _TABLA_H

#include<vector>

/**
 * @brief Esta clase representa una tabla de frecuencias de estadística descriptiva
 */
class tabla {
	private:
		std::vector<unsigned int> ni; ///< Frecuencias absolutas
	public:
		/**
		 * @brief Constructor por defecto
		 */
		tabla() {}
		/**
		 * @brief Constructor a partir de un conjunto de frecuencias absolutas
		 * @param frec Frecuencias absolutas
		 */
		tabla(const std::vector<unsigned int> &frec) { ni = frec; }
		/**
		 * @brief Consultor de las frecuencias absolutas
		 * @return Las frecuencias absolutas
		 */
		std::vector<unsigned int> get_ni() const { return ni; }
		/**
		 * @brief Sobreescribe el valor de las frecuencias absolutas
		 * @param new_ni Nuevas frecuencias absolutas
		 */
		void set_ni(const std::vector<unsigned int> &new_ni) { ni = new_ni; }
		/**
		 * @brief Calcula el tamaño de la población
		 * @return El tamaño de la población
		 */
		unsigned int get_poblacion() const;
		/**
		 * @brief Calcula las frecuencias absolutas acumuladas
		 * @return Las frecuencias acumuladas
		 */
		std::vector<unsigned int> get_Ni() const;
		/**
		 * @brief Calcula las frecuencias relativas
		 * @return Las frecuencias relativas
		 */
		std::vector<float> get_fi() const;
		/**
		 * @brief Calcula las frecuencias relativas acumuladas
		 * @return Las frecuencias relativas acumuladas
		 */
		std::vector<float> get_Fi() const;
};

#endif
